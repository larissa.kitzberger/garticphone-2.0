# garticphone-2.0

Team members: Miriam Hager (s2010629003), Larissa Kitzberger (s2010629005)

**IMPORTANT**
If you want to test the game on one device you have to use a different browser for each player. 
(The current user is stored in the local storage)
