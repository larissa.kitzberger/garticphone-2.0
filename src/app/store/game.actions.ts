import {Game} from '../types/game';

export class SetGame {
  static readonly type = '[Game] SetGame';

  constructor(public game: Game) {
  }
}

/**
 * increase current round or redirect to result page if it's the last round
 */
export class IncreaseCurrentRound {
  static readonly type = '[Game] IncreaseCurrentRound';

  constructor(public code: number) {
  }

}
