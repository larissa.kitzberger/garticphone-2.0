import {Action, NgxsOnInit, Selector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {Game} from '../types/game';
import {IncreaseCurrentRound, SetGame} from './game.actions';
import {DbService} from '../modules/shared/services/db.service';
import {StorageService} from '../modules/shared/services/storage.service';
import {Router} from '@angular/router';
import {ROUTING} from '../modules/shared/constants';
import firebase from 'firebase';

export interface GameStateModel {
  game: Game;
}

function getDefaultState(): Game {
  return {
    code: 0,
    id: '0',
    user: [],
    gameManager: {
      numberRounds: 0,
      currentRound: 0,
      finished: false,
      gameManagerStates: []
    }
  };
}


@State<Game>({
  name: 'gameState',
  defaults: getDefaultState()
})
@Injectable()
export class GameState implements NgxsOnInit {

  @Selector()
  static game(state: GameStateModel): Game {
    return state.game;
  }

  @Selector()
  static currentRound(state: GameStateModel): number {
    return state.game.gameManager.currentRound;
  }

  constructor(private dbService: DbService,
              private storageService: StorageService,
              private router: Router) {
  }

  async ngxsOnInit(context?: StateContext<GameStateModel>): Promise<any> {
    const gameCode = this.storageService.getGameCode();
    if (gameCode && context) {
      const game = await this.dbService.getGame(gameCode.toString());
      context?.dispatch(new SetGame(game));
      this.listenToCurrentRound(context, gameCode, game);
      this.listenToGameFinished(gameCode);
      this.listenToGameExists(gameCode);
    }
  }

  @Action(SetGame)
  setGame(context: StateContext<GameStateModel>, action: SetGame): void {
    context.patchState({
      game: action.game
    });
  }

  @Action(IncreaseCurrentRound)
  async increaseCurrentRound(context: StateContext<GameStateModel>, action: IncreaseCurrentRound): Promise<void> {
    const game = await this.dbService.getGame(action.code.toString());
    // already last round
    if (game.gameManager.currentRound === game.gameManager.numberRounds) {
      await this.dbService.setGameToFinished(game.code);
    } else {
      await this.dbService.increaseCurrentRound(game.code, game.gameManager.currentRound);
      game.gameManager.currentRound++;
      context.patchState({ game });
    }
  }

  /**
   * listen to changes on the current round
   * if the host changes it - save the round in the current game state and redirect user to round
   */
  private listenToCurrentRound(context: StateContext<GameStateModel>, code: number, game: Game): void {
    firebase.database().ref().child('games').child(code.toString()).child('gameManager')
      .child('currentRound').on('value', async snapshot => {
      if (snapshot.exists()) {
        game.gameManager.currentRound = snapshot.val();
        context.dispatch(new SetGame(game));
        await this.router.navigate([ROUTING.GAME, code, ROUTING.ROUND, snapshot.val()]);
      }
    });
  }

  /**
   * listen to host, when game is finished and redirect to result page
   */
  private listenToGameFinished(code: number): void {
    firebase.database().ref().child('games').child(code.toString()).child('gameManager')
      .child('finished').on('value', async snapshot => {
        if (snapshot.exists() && snapshot.val() === true) {
          await this.router.navigate([ROUTING.RESULT, code]);
        }
    });
  }

  /**
   * listen to the game code, if the game doesn't exist anymore, redirect to the lobby
   */
  private listenToGameExists(code: number): void {
    firebase.database().ref().child('games').child(code.toString()).on('value', async snapshot => {
      if (snapshot.val() === null) {
        await this.router.navigate([ROUTING.LOBBY]);
        location.reload();
      }
    });
  }

}
