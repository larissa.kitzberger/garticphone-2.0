import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ROUTING} from './modules/shared/constants';
import {IsGameFinishedGuard} from './guards/is-game-finished.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: ROUTING.LOBBY,
    pathMatch: 'full'
  },
  {
    path: ROUTING.LOBBY,
    loadChildren: () => import('./modules/lobby/lobby.module').then(m => m.LobbyModule)
  },
  {
    path: ROUTING.GAME,
    loadChildren: () => import('./modules/game/game.module').then(m => m.GameModule)
  },
  {
    path: ROUTING.RESULT,
    canActivate: [IsGameFinishedGuard],
    canActivateChild: [IsGameFinishedGuard],
    loadChildren: () => import('./modules/result/result.module').then(m => m.ResultModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
