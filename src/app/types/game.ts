import {User} from './user';

export type Game = {
  code: number;
  id: string;
  user: User[]
  gameManager: GameManager;
};

export type GameManager = {
  numberRounds: number;
  currentRound: number;
  finished: boolean;
  gameManagerStates: State[];
};

export type State = {
  initialStateId: string;
  descriptions: string[];
  drawings: string[];
};


