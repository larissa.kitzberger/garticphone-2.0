
export type User = {
  id: string;
  userName: string;
  avatar: string;
  host: boolean;
  initialStateId: string | null;
  currentStateId: string | null;
};
