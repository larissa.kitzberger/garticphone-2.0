import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserSetGuard} from '../../guards/user-set.guard';
import {RoundComponent} from './components/round/round.component';
import {ROUTING} from '../shared/constants';

const routes: Routes = [
  {
    // ToDo path: '' -> reroute to lobby?

    path: ':id',
    canActivate: [UserSetGuard],
    children: [
      {
        path: ROUTING.ROUND,
        children: [
          {
            path: ':id',
            component: RoundComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameRoutingModule { }
