import {Injectable} from '@angular/core';
import {DbService} from '../../shared/services/db.service';
import {User} from '../../../types/user';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(private dbService: DbService) {
  }

  async getNumberOfUsers(gameCode: string): Promise<number> {
    return await this.dbService.getNumberOfUsersGame(gameCode);
  }

  async storeDescription(gameId: string, currentStateId: string, round: number, description: string): Promise<void> {
    await this.dbService.storeDescription(gameId, currentStateId, round, description);
  }

  async storeDrawing(gameId: string, currentStateId: string, round: number, drawing: string): Promise<void> {
    await this.dbService.storeDrawing(gameId, currentStateId, round, drawing);
  }

  async getDescription(gameCode: string, stateId: string, round: string): Promise<string> {
    return this.dbService.getDescription(gameCode, stateId, round);
  }

  async getDrawing(gameCode: string, stateId: string, round: string): Promise<string> {
    return this.dbService.getDrawing(gameCode, stateId, round);
  }

  async getCurrentStateOfUser(gameId: string, userId: string): Promise<string> {
    return await this.dbService.getCurrentStateIdOfUser(gameId, userId);
  }

  async setCurrentStateOfUser(gameId: string, userId: string, currentStateId: string): Promise<void> {
    await this.dbService.setCurrentStateIdOfUser(gameId, userId, currentStateId);
  }
}
