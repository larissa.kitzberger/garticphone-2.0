import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {GameRoutingModule} from './game-routing.module';
import { DescribingComponent } from './components/describing/describing.component';
import {NgxsModule} from '@ngxs/store';
import {GameState} from '../../store/game.state';
import {NgxsLoggerPluginModule} from '@ngxs/logger-plugin';
import { RoundComponent } from './components/round/round.component';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import { TimerComponent } from './components/timer/timer.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { DrawingComponent } from './components/drawing/drawing.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';

@NgModule({
  declarations: [
    DescribingComponent,
    RoundComponent,
    TimerComponent,
    DrawingComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    GameRoutingModule,
    NgxsModule.forRoot([GameState]),
    NgxsLoggerPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
  ]
})
export class GameModule { }
