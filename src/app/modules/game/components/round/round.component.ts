import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Game} from '../../../../types/game';
import {Select, Store} from '@ngxs/store';
import {GameState} from '../../../../store/game.state';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {StorageService} from '../../../shared/services/storage.service';
import {User} from '../../../../types/user';
import {GameService} from '../../services/game.service';
import {IncreaseCurrentRound} from '../../../../store/game.actions';
import {ActivatedRoute} from '@angular/router';
import {FALLBACKPHRASES} from '../../../shared/constants';


@Component({
  selector: 'app-round',
  templateUrl: './round.component.html',
  styleUrls: ['./round.component.scss']
})
export class RoundComponent implements OnInit, OnDestroy {
  @Select(GameState.game)

  game$: Observable<Game> | undefined;
  public restartTimer$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  public game: Game | undefined = undefined;
  public roundType: 'describing' | 'drawing' = 'describing';
  public absolutNumRounds: number | undefined;
  public currentRound: number | undefined;
  public stop = false;
  public gameId: string | undefined;
  public loading = true;
  public currentUser: User | null = null;
  public previousData = '';


  activeRouteSubscription?: Subscription;
  gameSubscription?: Subscription;

  constructor(private storageService: StorageService,
              private gameService: GameService,
              private store: Store,
              private activatedRoute: ActivatedRoute) {
    this.currentUser = this.storageService.getUser();
    this.activeRouteSubscription = this.activatedRoute.params.subscribe(async () => {
      await this.ngOnInit();
    });
    this.playAudio();
  }

  ngOnDestroy(): void {
    this.gameSubscription?.unsubscribe();
    this.activeRouteSubscription?.unsubscribe();
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    this.gameSubscription = this.game$?.subscribe(result => {
      this.game = result;
      this.setRound();
      this.setRoundType();
      this.gameId = this.game?.code.toString();
    });
    await this.getPreviousData(this.roundType);
    this.stop = false;
    this.loading = false;
  }

  /**
   * sets absolut number of rounds
   * sets current round
   */
  async setRound(): Promise<void> {
    if (this.game && this.game.gameManager) {
      this.absolutNumRounds = this.game.gameManager.numberRounds;
      this.currentRound = this.game.gameManager.currentRound;
    }
  }

  /**
   * even number => drawing component
   * uneven number => describing component
   */
  async setRoundType(): Promise<void> {
    if (this.game) {
      if (this.game.gameManager.currentRound % 2 === 0) {
        this.roundType = 'drawing';
      } else {
        this.roundType = 'describing';
      }
    }
  }

  /**
   * changes to true if time is up
   */
  async stopCurrentRound(val: boolean): Promise<void> {
    this.stop = val;
    if (this.stop && this.game && this.currentUser && this.currentUser.initialStateId) {

      // if there is no entry in round 1 set random phrase
      if (this.currentRound === 1) {
        if (!await this.gameService.getDescription(this.game.code.toString(), this.currentUser.initialStateId, '0')) {
          await this.gameService.storeDescription(this.game.code.toString(), this.currentUser.initialStateId, 0, this.getRandomPhrase());
        }
      }
      // if current user is host then go to next round
      if (this.currentUser.host) {
        setTimeout(() => {
          if (this.game) {
            this.store.dispatch(new IncreaseCurrentRound(this.game.code));
          }
        }, 400);
      }
      // restart timer for all users
      if (this.currentRound && this.absolutNumRounds && this.currentRound < this.absolutNumRounds) {
        setTimeout(() => {
          this.restartTimer$.next(true);
        }, 100);
        this.restartTimer$.next(false);
      }
    }
  }

  /**
   * save the drawings or descriptions
   */
  async updateState(entry: string): Promise<void> {
    if (this.gameId && this.currentUser && this.currentRound) {

      // get id of user
      let userId = '0';
      if (!this.currentUser.host) {
        userId = this.currentUser.id;
      }
      // get current state of user
      const currentStateId = await this.gameService.getCurrentStateOfUser(this.gameId, userId);

      if (this.roundType === 'describing') {
        await this.gameService.storeDescription(this.gameId, currentStateId, this.currentRound - 1, entry);
      } else {
        await this.gameService.storeDrawing(this.gameId, currentStateId, this.currentRound - 1, entry);
      }
    }
  }

  /**
   * get previous data
   * set new current state of user
   */
  async getPreviousData(type: string): Promise<void> {
    if (this.game && this.gameId && this.currentRound && this.currentUser) {
      if (this.currentRound > 1) {
        // get id of user
        let userId = '0';
        if (!this.currentUser.host) {
          userId = this.currentUser.id;
        }

        const currentStateDB = await this.gameService.getCurrentStateOfUser(this.gameId, userId);
        // @ts-ignore, increase current state of user
        let currentState = currentStateDB * 1;
        if (currentState + 1 === this.absolutNumRounds) {
          currentState = 0;
        } else {
          currentState++;
        }

        // get previous data
        if (type === 'describing') {
          this.previousData = await this.gameService.getDrawing(this.gameId.toString(), currentState.toString(), (this.currentRound - 2)
            .toString());
        } else {
          this.previousData = await this.gameService.getDescription(this.gameId.toString(), currentState.toString(), (this.currentRound - 2)
            .toString());
        }

        // set new current state of user
        await this.gameService.setCurrentStateOfUser(this.gameId.toString(), userId, currentState.toString());
      }
    }
  }

  /**
   * returns a random phrase
   */
  getRandomPhrase(): string {
    return FALLBACKPHRASES[Math.floor(Math.random() * FALLBACKPHRASES.length)];
  }

  async playAudio(): Promise<void> {
    const audio = new Audio();
    audio.src = '../../../assets/audio/game_start.wav';
    audio.load();
    await audio.play();
  }
}
