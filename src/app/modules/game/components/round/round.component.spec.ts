import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoundComponent } from './round.component';
import {NgxsModule} from '@ngxs/store';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {GameState} from '../../../../store/game.state';
import {RouterTestingModule} from '@angular/router/testing';

xdescribe('RoundComponent', () => {
  let component: RoundComponent;
  let fixture: ComponentFixture<RoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoundComponent ],
      imports: [
        MatSnackBarModule,
        NgxsModule.forRoot([GameState]),
        RouterTestingModule
      ],
      providers: [
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
