import {
  AfterViewInit,
  Component,
  ElementRef,
  Output,
  ViewChild,
  EventEmitter,
  OnDestroy, Input
} from '@angular/core';
import {fromEvent, Subscription} from 'rxjs';
import {debounceTime, pairwise, switchMap, takeUntil, tap} from 'rxjs/operators';
import {animate, style, transition, trigger} from '@angular/animations';

type Position = {
  x: number,
  y: number
};

type ColorPosition = {
  prevPos: Position,
  currentPos: Position,
  color: string
};

@Component({
  selector: 'app-drawing',
  templateUrl: './drawing.component.html',
  styleUrls: ['./drawing.component.scss'],
  animations: [
    trigger('canvasEnter', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('200ms ease-in', style({ opacity: 1 })),
      ])
    ])
  ]
})
export class DrawingComponent implements AfterViewInit, OnDestroy {

  @ViewChild('canvas') canvas?: ElementRef<HTMLCanvasElement>;
  @Output() drawingChanged = new EventEmitter<string>();
  @Input() previousData: string | undefined;

  colors: string[] = ['#000000', '#7D786F', '#FFFFFF', '#57A135', '#D31B00', '#F4C917', '#9200D7', '#010AD6', '#915500'];
  currentColor = '';
  context: CanvasRenderingContext2D | undefined = undefined;

  savedDrawingSet: Set<ColorPosition> = new Set<ColorPosition>();
  oldWidth = 0;
  currentWidth = 0;

  drawingSubscription?: Subscription;
  saveDrawingSubscription?: Subscription;

  constructor() {
  }

  ngAfterViewInit(): void {
    this.context = this.canvas?.nativeElement?.getContext('2d') as CanvasRenderingContext2D;
    this.captureMouseEvents((window.innerWidth / 100) * 50, (window.innerHeight / 100) * 40);
    this.oldWidth = this.canvas?.nativeElement.clientWidth as number;

    window.addEventListener('resize', () => {
      this.currentWidth = this.canvas?.nativeElement.clientWidth as number;
      this.captureMouseEvents((window.innerWidth / 100) * 50, (window.innerHeight / 100) * 40);
    });
  }

  ngOnDestroy(): void {
    this.drawingSubscription?.unsubscribe();
    this.saveDrawingSubscription?.unsubscribe();
  }

  captureMouseEvents(width: number, height: number): void {
    const canvasElement: HTMLCanvasElement = this.canvas?.nativeElement as HTMLCanvasElement;
    canvasElement.width = width;
    canvasElement.height = height;

    this.handleWindowResize();

    this.drawingSubscription = fromEvent(canvasElement, 'mousedown').pipe(
      switchMap(() => {
        return fromEvent(canvasElement, 'mousemove').pipe(
          takeUntil(fromEvent(canvasElement, 'mouseup')),
          takeUntil(fromEvent(canvasElement, 'mouseleave')),
          pairwise()
        );
      })
    ).subscribe((result: any) => {
      const rectangle = canvasElement.getBoundingClientRect();
      const prevPos: Position = {
        x: result[0].clientX - rectangle.left,
        y: result[0].clientY - rectangle.top
      };
      const currentPos: Position = {
        x: result[1].clientX - rectangle.left,
        y: result[1].clientY - rectangle.top
      };

      this.draw(prevPos, currentPos, this.currentColor);
      this.savedDrawingSet.add({prevPos, currentPos, color: this.currentColor});
    });

    this.emitDrawingState(canvasElement);
  }

  draw(prevPos: Position, currentPos: Position, color: string): void {
    if (this.context) {
      this.context.strokeStyle = color;
      this.context.beginPath();
      if (prevPos) {
        this.context.moveTo(prevPos.x, prevPos.y);
        this.context.lineTo(currentPos.x, currentPos.y);
        this.context.stroke();
      }
    }
  }

  handleWindowResize(): void {
    if (this.savedDrawingSet.size > 0) {
      this.savedDrawingSet.forEach((entry: ColorPosition) => {
        const difference = (Math.abs(this.currentWidth - this.oldWidth));
        if (this.currentWidth < this.oldWidth) {
          entry.prevPos.x -= difference;
          entry.currentPos.x -= difference;
        } else {
          entry.prevPos.x += difference;
          entry.currentPos.x += difference;
        }
        this.draw(entry.prevPos, entry.currentPos, entry.color);
      });
      this.oldWidth = this.currentWidth;
    }
  }

  clearCanvas(): void {
    this.savedDrawingSet.clear();
    this.context?.clearRect(0, 0, (window.innerWidth / 100) * 80, (window.innerHeight / 100) * 50);
    this.drawingChanged.emit('');
  }

  emitDrawingState(canvasElement: HTMLCanvasElement): void {
    this.saveDrawingSubscription = fromEvent(canvasElement, 'mouseup').pipe(
      debounceTime(500),
      tap(() => this.drawingChanged.emit(this.canvas?.nativeElement.toDataURL()))
    ).subscribe();
  }
}
