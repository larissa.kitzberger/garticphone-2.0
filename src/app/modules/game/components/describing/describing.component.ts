import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {animate, style, transition, trigger} from '@angular/animations';
import {debounceTime, tap} from 'rxjs/operators';
import {FALLBACKPHRASES} from '../../../shared/constants';

@Component({
  selector: 'app-describing',
  templateUrl: './describing.component.html',
  styleUrls: ['./describing.component.scss'],
  animations: [
    trigger('inputEnter', [
      transition(':enter', [
        style({opacity: 0}),
        animate('200ms ease-in', style({opacity: 1})),
      ])
    ])
  ]
})
export class DescribingComponent implements OnInit {
  @Input() currentRound: number | undefined;
  @Input() previousData: string | undefined;
  @Output() descriptionChanged = new EventEmitter<string>();

  public descriptionFormGroup: FormGroup;
  public isSaved = false;
  public placeholder: string | undefined;


  constructor(private formBuilder: FormBuilder) {
    this.descriptionFormGroup = this.formBuilder.group({
      description: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    if (this.currentRound === 1) {
      this.placeholder = this.getRandomPhrase();
    } else {
      this.placeholder = 'Insert your description';
    }

    this.descriptionFormGroup.valueChanges.subscribe(() => {
      this.descriptionChanged.emit(this.descriptionFormGroup.controls.description.value);
    });
    this.descriptionFormGroup.valueChanges.pipe(
      debounceTime(500),
      tap(() => {
        this.descriptionChanged.emit(this.descriptionFormGroup.controls.description.value);
      })).subscribe();
  }

  /**
   * returns a random phrase
   */
  getRandomPhrase(): string {
    return FALLBACKPHRASES[Math.floor(Math.random() * FALLBACKPHRASES.length)];
  }

  /**
   * toggles description button
   * stores the description in the data base
   */
  setDescription(): void {
    if (this.isSaved) {
      this.isSaved = false;
      this.descriptionFormGroup.controls.description.enable();
    } else {
      this.isSaved = true;
      this.descriptionFormGroup.controls.description.disable();
    }
  }


}
