import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DescribingComponent } from './describing.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {By} from '@angular/platform-browser';

describe('DescribingComponent', () => {
  let component: DescribingComponent;
  let fixture: ComponentFixture<DescribingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DescribingComponent ],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DescribingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('first round', async () => {
    expect(component).toBeTruthy();
    component.currentRound = 1;
    component.stop = false;
    component.previousData = undefined;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('h1')).nativeElement.textContent.trim())
      .toEqual('Start a Story!');
  });
});
