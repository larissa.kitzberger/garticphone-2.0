import {Component, OnInit, Output, EventEmitter, Input, OnDestroy} from '@angular/core';
import {BehaviorSubject, Subscription} from 'rxjs';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit, OnDestroy {
  public timer = 0;
  public spinnerVal = 0;
  @Output() timeIsUp = new EventEmitter<boolean>();
  @Input() restart: BehaviorSubject<boolean> | undefined;
  timerSubscription?: Subscription;

  ngOnInit(): void {
    this.startTimer(1);
    this.timeIsUp.emit(false);

    this.timerSubscription =  this.restart?.subscribe(value => {
      if (value) {
        this.startTimer(1);
      }
    });
  }

  ngOnDestroy(): void {
    this.timerSubscription?.unsubscribe();
  }

  /**
   * starts the timer
   */
  startTimer(minute: number): void {
    let seconds = minute * 60;

    const calcTime = setInterval(() => {
      seconds--;
      this.timer = seconds;
      this.spinnerVal = seconds * 1.67;

      if (seconds === 5) {
        this.playAudio();
      }

      if (seconds === 0) {
        clearInterval(calcTime);
        this.timeIsUp.emit(true);
      }
    }, 1000);
  }

 async playAudio(): Promise<void> {
    const audio = new Audio();
    audio.src = '../../../assets/audio/timer.wav';
    audio.load();
    await audio.play();
  }


}
