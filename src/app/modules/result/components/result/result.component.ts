import { Component, OnInit } from '@angular/core';
import {Game} from '../../../../types/game';
import {LobbyService} from '../../../lobby/services/lobby.service';
import {ActivatedRoute, Router} from '@angular/router';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';
import {User} from '../../../../types/user';
import {UserService} from '../../../shared/services/user.service';
import {ResultService} from '../../services/result.service';
import {StorageService} from '../../../shared/services/storage.service';
import {EMPTY_IMAGE} from '../../../shared/constants';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  animations: [
    trigger('listAnimation', [
      transition('* => *', [
        query(':leave', [
          stagger(100, [
            animate('0.5s', style({ opacity: 0 }))
          ])
        ], { optional: true }),
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('1s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})
export class ResultComponent implements OnInit {
  public game: Game | undefined = undefined;
  public code = 0;
  public rounds: number[] = [];
  public currentResult = 0;
  public currentUser: User | undefined = undefined;
  public isHost = false;
  public emptyImage: string = EMPTY_IMAGE.IMAGE;


  constructor(private lobbyService: LobbyService,
              private activatedRoute: ActivatedRoute,
              private userService: UserService,
              private resultService: ResultService,
              private storageService: StorageService,
              private router: Router) {
    this.code = this.activatedRoute.snapshot.params.id;
  }

  async ngOnInit(): Promise<void> {
    this.game = await this.lobbyService.getGame(this.code.toString());
    this.rounds.length = this.game.gameManager.numberRounds;
    await this.updateCurrentUser();
    this.isHost = this.storageService.isHost();
    this.playAudio();
  }

  async showPreviousResult(element: HTMLElement): Promise<void> {
    this.currentResult--;
    await this.updateCurrentUser();
    element.scrollIntoView();
  }

  async showNextResult(element: HTMLElement): Promise<void> {
    this.currentResult++;
    await this.updateCurrentUser();
    element.scrollIntoView();
  }

  async leaveGame(): Promise<void> {
    await this.resultService.clearGame(this.code);
    await this.router.navigate(['/']);
  }

  private async updateCurrentUser(): Promise<void> {
    this.currentUser = await this.resultService.getUserByInitialStateId(this.code, this.currentResult.toString());
  }

  async playAudio(): Promise<void> {
    const audio = new Audio();
    audio.src = '../../../assets/audio/applause.wav';
    audio.load();
    await audio.play();
  }

}
