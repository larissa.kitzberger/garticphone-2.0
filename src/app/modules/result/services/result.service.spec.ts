import { TestBed } from '@angular/core/testing';

import { ResultService } from './result.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {environment} from '../../../../environments/environment';
import {AngularFireModule} from '@angular/fire';

xdescribe('ResultService', () => {
  let service: ResultService;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase, Math.random().toString()),
        MatSnackBarModule
      ],
      providers: [
      ]
    });
    service = TestBed.inject(ResultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
