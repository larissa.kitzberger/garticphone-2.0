import { Injectable } from '@angular/core';
import {User} from '../../../types/user';
import {DbService} from '../../shared/services/db.service';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private dbService: DbService) { }

  public async getUserByInitialStateId(gameCode: number, initialStateId: string): Promise<User | undefined> {
    return await this.dbService.getUserByInitialStateId(gameCode, initialStateId);
  }

  public async clearGame(gameCode: number): Promise<void> {
    await this.dbService.clearGame(gameCode);
  }
}
