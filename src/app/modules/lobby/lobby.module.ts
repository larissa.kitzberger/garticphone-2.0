import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LobbyRoutingModule } from './lobby-routing.module';
import { StartComponent } from './components/start/start.component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDialogModule} from '@angular/material/dialog';
import { LobbyComponent } from './components/lobby/lobby.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    StartComponent,
    LobbyComponent
  ],
  imports: [
    SharedModule,
    LobbyRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatGridListModule,
    MatDialogModule,
    SharedModule,
    CommonModule
  ]
})
export class LobbyModule { }
