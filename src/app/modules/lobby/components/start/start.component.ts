import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../../../types/user';
import { v4 as uuidv4 } from 'uuid';
import {Game} from '../../../../types/game';
import {LobbyService} from '../../services/lobby.service';
import {AVATAR} from '../../../shared/constants';
import {SnackBarService} from '../../../shared/services/snack-bar.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private lobbyService: LobbyService,
              private snackBarService: SnackBarService) {
    this.userFormGroup = this.formBuilder.group({
      userName: ['', [Validators.required]],
      code: ['', [Validators.pattern(/^-?(0|[1-9]\d*)?$/)]]
    });
  }

  public availableAvatars: string[] = [AVATAR['2'], AVATAR['3'], AVATAR['4'], AVATAR['5'],
    AVATAR['6'], AVATAR['7'], AVATAR['8'], AVATAR['1']];

  public selectedAvatar: string = AVATAR['1'];

  public userFormGroup: FormGroup;

  private static generateGameCode(): number {
    return Math.abs(Math.floor(
      Math.random() * (1 - 10000) + 1
    ));
  }

  ngOnInit(): void {
  }

  /**
   * change the selected avatar to the next one in the array
   * put the used one at the end of the array
   */
  shuffleAvatars(): void {
    this.selectedAvatar = this.availableAvatars[0];
    this.availableAvatars.shift();
    this.availableAvatars.push(this.selectedAvatar);
  }

  async createGame(): Promise<void> {
    const user: User = {
      id: uuidv4(),
      userName: this.userFormGroup.get('userName')?.value,
      avatar: this.selectedAvatar,
      host: true,
      initialStateId: '0',
      currentStateId: '0'

    };
    const game: Game = {
      code: StartComponent.generateGameCode(),
      id: uuidv4(),
      user: [user],
      gameManager: {
        numberRounds: 0,
        currentRound: 0,
        finished: false,
        gameManagerStates: []
      },
    };
    await this.lobbyService.createGame(game);
    await this.lobbyService.initGame(user, game);
    await this.router.navigate(['/lobby/' + game.code]);
  }

  async joinGame(): Promise<void> {
    const game: Game = {
      code: this.userFormGroup.get('code')?.value,
      id: uuidv4(),
      user: [],
      gameManager: {
        numberRounds: 0,
        currentRound: 0,
        finished: false,
        gameManagerStates: []
      }
    };
    const user: User = {
      id: uuidv4(),
      userName: this.userFormGroup.get('userName')?.value,
      avatar: this.selectedAvatar,
      host: false,
      initialStateId: await this.lobbyService.getInitialState(game.code.toString()),
      currentStateId: await this.lobbyService.getInitialState(game.code.toString())

    };
    await this.lobbyService.joinGame(game.code, user).then(async () => {
      await this.lobbyService.initGame(user, game);
      await this.router.navigate(['/lobby/' + game.code]);
    }).catch(() => {
      this.snackBarService.showErrorSnackBar('A game with this code does not exist.');
    });

  }

}
