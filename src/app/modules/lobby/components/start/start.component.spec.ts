import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StartComponent } from './start.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import firebase from 'firebase';
import {environment} from '../../../../../environments/environment';
import App = firebase.app.App;
import {By} from '@angular/platform-browser';
import SpyObj = jasmine.SpyObj;
import {LobbyService} from '../../services/lobby.service';
import {SnackBarService} from '../../../shared/services/snack-bar.service';
import {DebugElement} from '@angular/core';


describe('StartComponent', () => {
  let component: StartComponent;
  let fixture: ComponentFixture<StartComponent>;
  let firebaseApp: App;
  let lobbyServiceSpy: SpyObj<LobbyService>;
  let snackBarServiceSpy: SpyObj<SnackBarService>;

  beforeAll(() => {
    firebaseApp = firebase.initializeApp(environment.firebase);
  });

  beforeEach(async () => {
    lobbyServiceSpy = jasmine.createSpyObj<LobbyService>(['createGame', 'initGame', 'joinGame']);
    snackBarServiceSpy = jasmine.createSpyObj<SnackBarService>(['showErrorSnackBar']);

    await TestBed.configureTestingModule({
      declarations: [ StartComponent ],
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      providers: [
        {provide: LobbyService, useValue: lobbyServiceSpy},
        {provide: SnackBarService, useValue: snackBarServiceSpy}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    firebaseApp.delete();
  });

  it('template', () => {
    expect(component).toBeTruthy();

    // avatar
    const avatar: DebugElement = fixture.debugElement.query(By.css('.image img'));
    expect(avatar.attributes.src).toEqual('./assets/avatars/avatar-1.png');
    component.shuffleAvatars();
    fixture.detectChanges();
    expect(avatar.attributes.src).toEqual('./assets/avatars/avatar-2.png');
    expect(component.availableAvatars[0]).toEqual('avatar-3.png');
  });

  it('form validation', () => {
    const buttons: DebugElement[] = fixture.debugElement.queryAll(By.css('button'));

    expect(component.userFormGroup.valid).toBeFalse();
    component.userFormGroup.get('userName')?.setValue('Ivy');
    fixture.detectChanges();
    expect(component.userFormGroup.valid).toBeTrue();
    expect(buttons[0].nativeElement.disabled).toBeFalse();
    expect(buttons[1].nativeElement.disabled).toBeTrue();

    component.userFormGroup.get('code')?.setValue('invalid value');
    fixture.detectChanges();
    expect(component.userFormGroup.valid).toBeFalse();
    component.userFormGroup.get('code')?.setValue(1234);
    fixture.detectChanges();
    expect(component.userFormGroup.valid).toBeTrue();
  });

  it('createGame', () => {
    component.userFormGroup.get('userName')?.setValue('Ivy');
    fixture.detectChanges();

    component.createGame();
    expect(lobbyServiceSpy.createGame).toHaveBeenCalled();
  });
});
