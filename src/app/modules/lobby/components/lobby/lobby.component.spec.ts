import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LobbyComponent } from './lobby.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import firebase from 'firebase';
import App = firebase.app.App;
import {environment} from '../../../../../environments/environment';
import {SnackBarService} from '../../../shared/services/snack-bar.service';
import SpyObj = jasmine.SpyObj;
import {StorageService} from '../../../shared/services/storage.service';
import {LobbyService} from '../../services/lobby.service';

describe('LobbyComponent', () => {
  let component: LobbyComponent;
  let fixture: ComponentFixture<LobbyComponent>;
  let firebaseApp: App;
  let snackBarServiceSpy: SpyObj<SnackBarService>;
  let storageServiceSpy: SpyObj<StorageService>;
  let lobbyServiceSpy: SpyObj<LobbyService>;


  beforeAll(() => {
    firebaseApp = firebase.initializeApp(environment.firebase);
  });

  beforeEach(async () => {
    snackBarServiceSpy = jasmine.createSpyObj<SnackBarService>(['showSuccessSnackBar']);
    storageServiceSpy = jasmine.createSpyObj<StorageService>(['getGameCode', 'getUser']);
    lobbyServiceSpy = jasmine.createSpyObj<LobbyService>(['listenToGameStart',
      'listenToUserChanges', 'getNumberOfUsers', 'initGameManager', 'increaseCurrentRound']);

    await TestBed.configureTestingModule({
      declarations: [ LobbyComponent ],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        RouterTestingModule
      ],
      providers: [
        {provide: SnackBarService, useValue: snackBarServiceSpy},
        {provide: StorageService, useValue: storageServiceSpy},
        {provide: LobbyService, useValue: lobbyServiceSpy},
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    storageServiceSpy.getGameCode.and.returnValue(1234);

    fixture = TestBed.createComponent(LobbyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterAll(() => {
    firebaseApp.delete();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadUser', () => {
    component.loadUser();
    expect(lobbyServiceSpy.listenToUserChanges).toHaveBeenCalled();
  });

  it('startGame', (done) => {
    component.startGame().then(() => {
      expect(lobbyServiceSpy.getNumberOfUsers).toHaveBeenCalled();
      expect(lobbyServiceSpy.increaseCurrentRound).toHaveBeenCalled();
      done();
    });

  });
});
