import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../../types/user';
import {Subscription} from 'rxjs';
import {LobbyService} from '../../services/lobby.service';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';
import {StorageService} from '../../../shared/services/storage.service';
import {SnackBarService} from '../../../shared/services/snack-bar.service';
import {GameManager, State} from '../../../../types/game';
import {animate, query, stagger, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss'],
  animations: [
    trigger('userAdded', [
      transition('* => *', [
        query(':enter', [
          style({ opacity: 0 }),
          stagger(100, [
            animate('1s', style({ opacity: 1 }))
          ])
        ], { optional: true })
      ])
    ])
  ]
})
export class LobbyComponent implements OnInit, OnDestroy {

  public userMap: Map<string, User> = new Map<string, User>();
  public currentUser: User | null = null;
  public code: number;

  public userSubscription: Subscription | null = null;
  public userMapSubscription: Subscription | null = null;

  public loading = true;

  constructor(private userService: UserService,
              private storageService: StorageService,
              private snackBarService: SnackBarService,
              private lobbyService: LobbyService,
              private activatedRoute: ActivatedRoute) {
    this.code = this.activatedRoute.snapshot.params.id;
  }

  async ngOnInit(): Promise<void> {
    await this.lobbyService.listenToGameStart(this.code);
    await this.loadUser();
    this.currentUser = this.storageService.getUser();
    this.userSubscription = this.userService.CurrentUser$.subscribe(response => {
      if (response.userName.length === 0) {
        const savedUser: User | null = this.storageService.getUser();
        if (savedUser) {
          this.userService.setCurrentUser(savedUser);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.userSubscription?.unsubscribe();
    this.userMapSubscription?.unsubscribe();
  }

  trackById(index: number, user: { key: string, value: User }): string {
    return user.value.id;
  }

  async loadUser(): Promise<void> {
    this.userMapSubscription = this.userService.CurrentUserMap$.subscribe(response => {
      this.userMap = response;
      this.loading = false;
    });
    await this.lobbyService.listenToUserChanges(this.code);
  }

  async copyLink(): Promise<void> {
    const code: number | null = this.storageService.getGameCode();
    if (code) {
      if (navigator.clipboard) {
        await navigator.clipboard.writeText(code.toString());
      }
      this.snackBarService.showSuccessSnackBar('The game link has been copied!');
    }
  }

  /**
   * host starts the game
   * number rounds = number of users
   */
  async startGame(): Promise<void> {
    const roundsNumber = await this.lobbyService.getNumberOfUsers(this.code?.toString());
    const numUsers = await this.lobbyService.getNumberOfUsers(this.code?.toString());
    const states = [];

    for (let i = 0; i < numUsers; i++) {
      const state: State = {
        initialStateId: i.toString(),
        descriptions: [],
        drawings: [],
      };
      states.push(state);
    }

    const gameManager: GameManager = {
      numberRounds: roundsNumber,
      currentRound: 1,
      finished: false,
      gameManagerStates: states
    };


    await this.lobbyService.initGameManager(this.code?.toString(), gameManager);
    await this.lobbyService.increaseCurrentRound(this.code, 0);
  }


}
