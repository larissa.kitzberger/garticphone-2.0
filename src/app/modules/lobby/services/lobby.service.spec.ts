import { TestBed } from '@angular/core/testing';
import { LobbyService } from './lobby.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {RouterTestingModule} from '@angular/router/testing';
import firebase from 'firebase';
import App = firebase.app.App;
import {environment} from '../../../../environments/environment';

describe('LobbyService', () => {
  let service: LobbyService;
  let firebaseApp: App;

  beforeAll(() => {
    firebaseApp = firebase.initializeApp(environment.firebase);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        RouterTestingModule
      ]
    });
    service = TestBed.inject(LobbyService);
  });

  afterAll(() => {
    firebaseApp.delete();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
