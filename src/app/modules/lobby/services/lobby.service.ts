import {Injectable} from '@angular/core';
import {Game, GameManager} from '../../../types/game';
import {User} from '../../../types/user';
import {UserService} from '../../shared/services/user.service';
import {DbService} from '../../shared/services/db.service';
import {StorageService} from '../../shared/services/storage.service';
import {ROUTING} from '../../shared/constants';
import firebase from 'firebase';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LobbyService {

  constructor(private dbService: DbService,
              private storageService: StorageService,
              private userService: UserService,
              private router: Router) {
  }

  async createGame(game: Game): Promise<void> {
    await this.dbService.createGame(game);
  }

  async joinGame(code: number, user: User): Promise<void> {
    const game: Game = await this.dbService.getGame(code.toString());
    if (game) {
      await this.dbService.joinGame(code, user);
    } else {
      throw new Error();
    }
  }

  async listenToUserChanges(code: number): Promise<void> {
    await this.dbService.listenToUserChanges(code);
  }

  /**
   * listen to changes on the current round
   * if the host changes it - save the round in the current game state and redirect user to round
   */
  listenToGameStart(code: number): void {
    firebase.database().ref().child('games').child(code.toString()).child('gameManager')
      .child('currentRound').on('value', async snapshot => {
      if (snapshot.exists() && snapshot.val() === 1) {
        await this.router.navigate([ROUTING.GAME, code, ROUTING.ROUND, 1]);
      }
    });
  }

  /**
   * save current user, gameCode in localStorage
   * save current user in observable
   */
  async initGame(user: User, game: Game): Promise<void> {
    this.storageService.saveUser(user);
    this.storageService.saveGameCode(game.code);
    this.storageService.saveGameId(game.id);
    this.userService.setCurrentUser(user);
  }

  async initGameManager(gameId: string, gameManager: GameManager): Promise<void> {
    await this.dbService.createGameManager(gameId, gameManager);
  }

  async getInitialState(gameId: string): Promise<string> {
    return await this.dbService.getInitialState(gameId);
  }

  async increaseCurrentRound(code: number, currentRound: number): Promise<void> {
    await this.dbService.increaseCurrentRound(code, currentRound);
  }

  async getGame(gameCode: string): Promise<Game> {
    return await this.dbService.getGame(gameCode);
  }

  async getNumberOfUsers(gameCode: string): Promise<number> {
    return await this.dbService.getNumberOfUsersGame(gameCode);
  }
}
