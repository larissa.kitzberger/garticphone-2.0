import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {StartComponent} from './components/start/start.component';
import {LobbyComponent} from './components/lobby/lobby.component';
import {UserSetGuard} from '../../guards/user-set.guard';

const routes: Routes = [
  {
    path: '',
    component: StartComponent
  },
  {
    path: ':id',
    canActivate: [UserSetGuard],
    component: LobbyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LobbyRoutingModule { }
