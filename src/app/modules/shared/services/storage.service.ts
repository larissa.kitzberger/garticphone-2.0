import { Injectable } from '@angular/core';
import {STORAGE} from '../constants';
import {User} from '../../../types/user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  saveUser(user: User): void {
    localStorage.setItem(STORAGE.USER, JSON.stringify(user));
  }

  getUser(): User | null {
    const result = localStorage.getItem(STORAGE.USER);
    if (result) {
      return JSON.parse(result);
    }
    return null;
  }

  isHost(): boolean {
    const result = localStorage.getItem(STORAGE.USER);
    if (result) {
      const user: User = JSON.parse(result);
      return user.host;
    }
    return false;
  }

  saveGameCode(code: number): void {
    localStorage.setItem(STORAGE.GAMECODE, code.toString());
  }

  getGameCode(): number | null {
    const result: string | null = localStorage.getItem(STORAGE.GAMECODE);
    if (result) {
      return result as unknown as number;
    }
    return null;
  }

  saveGameId(id: string): void {
    localStorage.setItem(STORAGE.GAMEID, id);
  }
}
