import { TestBed } from '@angular/core/testing';
import { DbService } from './db.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BehaviorSubject} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {environment} from '../../../../environments/environment';
import firebase from 'firebase';
import App = firebase.app.App;


describe('DbService', () => {
  let service: DbService;
  let firebaseApp: App;


  const angularFireAuthStub = {
    authState: new BehaviorSubject({}),
    auth: {
      signInWithPopup: jasmine.createSpy('signInWithPopup')
    },
  };

  beforeAll(() => {
    firebaseApp = firebase.initializeApp(environment.firebase);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule
      ],
      providers: [
        { provide: AngularFireAuth, useValue: angularFireAuthStub },
      ]
    });
    service = TestBed.inject(DbService);
  });

  afterAll(() => {
    firebaseApp?.delete();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
