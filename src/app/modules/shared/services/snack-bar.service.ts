import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) { }

  showSuccessSnackBar(message: string): void {
    this.snackBar.open(message, '', {duration: 3000});
  }

  showErrorSnackBar(message: string): void {
    this.snackBar.open(message, '', {duration: 3000});
  }

  showCommonError(): void {
    this.snackBar.open('Oups an error occurred! Please try again later.', '', {duration: 3000});
  }
}
