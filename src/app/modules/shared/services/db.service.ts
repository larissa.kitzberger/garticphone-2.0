import {Injectable} from '@angular/core';
import firebase from 'firebase';
import {SnackBarService} from './snack-bar.service';
import {UserService} from './user.service';
import {Game, GameManager} from '../../../types/game';
import {User} from '../../../types/user';
import Reference = firebase.database.Reference;

@Injectable({
  providedIn: 'root'
})
export class DbService {

  public databaseReference: Reference = firebase.database().ref();

  constructor(private snackBarService: SnackBarService,
              private userService: UserService) {
  }

  /**
   * create a new game at position game.code
   */
  async createGame(game: Game): Promise<void> {
    this.databaseReference.child('games').child(game.code.toString()).set(game).then(() => {
      this.snackBarService.showSuccessSnackBar('A new game has been created successfully!');
    }).catch(error => {
      console.error(error);
      this.snackBarService.showCommonError();
    });
  }

  /**
   * create a new game manager for the game
   */
  async createGameManager(gameId: string, gameManager: GameManager): Promise<void> {
    this.databaseReference.child('games').child(gameId).child('gameManager').set(gameManager).then(() => {
    }).catch(error => {
      console.error(error);
    });
  }

  /**
   * add user to existing game
   */
  async joinGame(code: number, user: User): Promise<void> {
    this.databaseReference.child('games').child(code.toString()).child('user').child(user.id)
      .set(user).then(() => {
      this.snackBarService.showSuccessSnackBar('You successfully entered the game!');
    }).catch(error => {
      console.error(error);
      this.snackBarService.showCommonError();
    });
  }

  /**
   * get current game
   */
  async getGame(gameCode: string): Promise<Game> {
    return await this.databaseReference.child('games').child(gameCode).get().then(snapshot => {
      if (snapshot.exists()) {
        return snapshot.val();
      }
    }).catch(error => {
      console.error(error);
    });
  }

  async getNumberOfUsersGame(gameId: string): Promise<number> {
    return this.databaseReference.child('games').child(gameId).child('user').get().then(snapshot => {
      if (snapshot.exists()) {
        return snapshot.numChildren();
      }
      return -1;
    }).catch(error => {
      console.error(error);
      return -1;
    });
  }

  async listenToUserChanges(code: number): Promise<void> {
    this.databaseReference.child('games').child(code.toString()).child('user')
      .on('value', snapshot => {
        if (snapshot.exists()) {
          const user: Map<string, User> = new Map<string, User>();
          snapshot.forEach(childSnapshot => {
            user.set(childSnapshot.key as string, childSnapshot.val());
          });
          this.userService.setCurrentUserMap(user);
        }
      });
  }

  /**
   * increase the currentRound for the game -> all other users are redirected
   */
  async increaseCurrentRound(code: number, currentRound: number): Promise<void> {
    const increased = currentRound + 1;
    await this.databaseReference.child('games').child(code.toString()).child('gameManager').child('currentRound')
      .set(increased).catch(error => {
        console.error(error);
        this.snackBarService.showCommonError();
      });
  }


  /**
   * gets the initial state of an user
   */
  async getInitialState(gameCode: string): Promise<string> {
    return this.databaseReference.child('games').child(gameCode).child('user').get().then(snapshot => {
      if (snapshot.exists()) {
        const size = snapshot.numChildren();
        return size.toString();
      }
      return '-1';
    }).catch(error => {
      console.error(error);
      return '-1';
    });
  }

  /**
   * returns the User according to the stateId (current sentence / drawing results)
   */
  async getUserByInitialStateId(gameCode: number, initialStateId: string): Promise<User | undefined> {
    return this.databaseReference.child('games').child(gameCode.toString()).child('user').get().then(snapshot => {
      if (snapshot.exists()) {
        const allUser: User[] = Object.values(snapshot.val());
        return allUser.find((element: User) => element.initialStateId === initialStateId);
      } else {
        return undefined;
      }
    }).catch(error => {
      console.error(error);
      return undefined;
    });
  }

  async getCurrentStateIdOfUser(gameCode: string, userId: string): Promise<string> {
    return await this.databaseReference.child('games').child(gameCode).child('user').child(userId).child('currentStateId').get()
      .then(snapshot => {
        return snapshot.val();
      });
  }

  async setCurrentStateIdOfUser(gameCode: string, userId: string, currentStateId: string): Promise<void> {
    await this.databaseReference.child('games').child(gameCode).child('user').child(userId).child('currentStateId').set(currentStateId);
  }

  /**
   * stores the description in the data base
   */
  async storeDescription(gameId: string, stateId: string, round: number, description: string): Promise<void> {
    await this.databaseReference.child('games').child(gameId).child('gameManager')
      .child('gameManagerStates').child(stateId).child('descriptions').child(round.toString())
      .set(description).catch(error => {
        console.error(error);
        this.snackBarService.showCommonError();
      });
  }

  /**
   * stores the drawing in the data base
   */
  async storeDrawing(gameCode: string, stateId: string, round: number, drawing: string): Promise<void> {
    await this.databaseReference.child('games').child(gameCode).child('gameManager')
      .child('gameManagerStates').child(stateId).child('drawings').child(round.toString())
      .set(drawing).catch(error => {
        console.error(error);
        this.snackBarService.showCommonError();
      });
  }

  /**
   * gets the drawing from the data base
   */
  async getDescription(gameCode: string, stateId: string, round: string): Promise<string> {
    return await this.databaseReference.child('games').child(gameCode).child('gameManager').child('gameManagerStates').child(stateId).child('descriptions').child(round).get().then(snapshot => {
      return snapshot.val();
    });
  }

  /**
   * gets the drawing from the data base
   */
  async getDrawing(gameCode: string, stateId: string, round: string): Promise<string> {
    return await this.databaseReference.child('games').child(gameCode).child('gameManager').child('gameManagerStates').child(stateId).child('drawings').child(round).get().then(snapshot => {
      return snapshot.val();
    });
  }

  /**
   * host sets the game after the last round to finished, so all the other users are redirected to the result page
   */
  async setGameToFinished(gameCode: number): Promise<void> {
    await this.databaseReference.child('games').child(gameCode.toString()).child('gameManager')
      .child('finished').set(true).then(() => {
        // success
      }).catch(error => {
        console.error(error);
      });
  }

  /**
   * check, if game is already finished
   */
  async isGameFinished(gameCode: number): Promise<boolean> {
    return await this.databaseReference.child('games').child(gameCode.toString()).child('gameManager')
      .child('finished').get().then(snapshot => {
        return snapshot.exists() && snapshot.val() === true;
      })
      .catch(error => {
        console.error(error);
        return false;
      });
  }

  /**
   * the host can click on "finish game" on the result page, which deletes the game from the database
   */
  async clearGame(gameCode: number): Promise<void> {
    await this.databaseReference.child('games').child(gameCode.toString()).remove(() => {
      // success
    });
  }


}
