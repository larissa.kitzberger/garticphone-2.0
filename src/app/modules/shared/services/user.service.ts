import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../../../types/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private currentUser$: BehaviorSubject<User> = new BehaviorSubject<User>({
    id: '0',
    avatar: '',
    host: false,
    userName: '',
    initialStateId: null,
    currentStateId: null
  });
  private currentUserArray$: BehaviorSubject<Map<string, User>> =
    new BehaviorSubject<Map<string, User>>(new Map<string, User>());

  public get CurrentUser$(): Observable<User> {
    return this.currentUser$.asObservable();
  }

  public get CurrentUserMap$(): Observable<Map<string, User>> {
    return this.currentUserArray$.asObservable();
  }

  constructor() { }

  public setCurrentUser(user: User): void {
    this.currentUser$.next(user);
  }

  public setCurrentUserMap(user: Map<string, User>): void {
    this.currentUserArray$.next(user);
  }
}
