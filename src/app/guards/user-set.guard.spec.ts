import { TestBed } from '@angular/core/testing';

import { UserSetGuard } from './user-set.guard';
import SpyObj = jasmine.SpyObj;
import {StorageService} from '../modules/shared/services/storage.service';

describe('UserSetGuard', () => {
  let guard: UserSetGuard;
  let storageServiceSpy: SpyObj<StorageService>;

  beforeEach(() => {
    storageServiceSpy = jasmine.createSpyObj<StorageService>(['getUser']);

    TestBed.configureTestingModule({});
    guard = TestBed.inject(UserSetGuard);
  });

  it('guard - false', (done) => {
    expect(guard).toBeTruthy();

    storageServiceSpy.getUser.and.returnValue(null);
    expect(guard.canActivate()).toBeFalse();
    done();
  });

  it('guard - true', (done) => {
    storageServiceSpy.getUser.and.returnValue({
      avatar: '',
      currentStateId: 'id',
      host: false,
      id: '',
      initialStateId: 'id',
      userName: ''
    });
    expect(guard.canActivate()).toBeFalse();
    done();
  });
});
