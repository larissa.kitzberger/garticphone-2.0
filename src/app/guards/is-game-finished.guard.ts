import { Injectable } from '@angular/core';
import {CanActivate, CanActivateChild} from '@angular/router';
import {DbService} from '../modules/shared/services/db.service';
import {StorageService} from '../modules/shared/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class IsGameFinishedGuard implements CanActivate, CanActivateChild {

  constructor(private dbService: DbService,
              private storageService: StorageService) {
  }

  async canActivate(): Promise<boolean> {
    return await this.check();
  }

  async canActivateChild(): Promise<boolean> {
    return await this.check();
  }

  async check(): Promise<boolean> {
    const gameCode = await this.storageService.getGameCode();
    if (gameCode) {
      return await this.dbService.isGameFinished(gameCode);
    } else {
      return false;
    }
  }

}
