import { TestBed } from '@angular/core/testing';

import { IsGameFinishedGuard } from './is-game-finished.guard';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import firebase from 'firebase';
import {environment} from '../../environments/environment';
import App = firebase.app.App;
import SpyObj = jasmine.SpyObj;
import {StorageService} from '../modules/shared/services/storage.service';
import {DbService} from '../modules/shared/services/db.service';

describe('IsGameFinishedGuard', () => {
  let guard: IsGameFinishedGuard;
  let firebaseApp: App;
  let storageServiceSpy: SpyObj<StorageService>;
  let dbServiceSpy: SpyObj<DbService>;

  beforeAll(() => {
    firebaseApp = firebase.initializeApp(environment.firebase);
  });

  beforeEach(() => {
    storageServiceSpy = jasmine.createSpyObj<StorageService>(['getGameCode']);
    dbServiceSpy = jasmine.createSpyObj<DbService>(['isGameFinished']);

    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule
      ]
    });
    guard = TestBed.inject(IsGameFinishedGuard);
  });

  afterAll(() => {
    firebaseApp.delete();
  });

  it('guard - false', (done) => {
    expect(guard).toBeTruthy();
    storageServiceSpy.getGameCode.and.returnValue(1234);
    dbServiceSpy.isGameFinished.and.returnValue(Promise.resolve(false));
    guard.canActivate().then(result => {
      expect(result).toBeFalse();
    });
    guard.canActivateChild().then(result => {
      expect(result).toBeFalse();
      done();
    });
  });

  it('guard - true', (done) => {
    storageServiceSpy.getGameCode.and.returnValue(654);
    dbServiceSpy.isGameFinished.and.returnValue(Promise.resolve(true));
    guard.canActivate().then(result => {
      expect(result).toBeFalse();
    });
    guard.canActivateChild().then(result => {
      expect(result).toBeFalse();
      done();
    });
  });
});
