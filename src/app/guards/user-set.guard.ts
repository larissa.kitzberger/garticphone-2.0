import { Injectable } from '@angular/core';
import { CanActivate,  UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {User} from '../types/user';
import {StorageService} from '../modules/shared/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserSetGuard implements CanActivate {

  constructor(private storageService: StorageService) {}

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const user: User | null = this.storageService.getUser();
    return !!user;
  }

}
